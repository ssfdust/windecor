#include "deco-theme.hpp"
#include <wayfire/core.hpp>
#include <wayfire/opengl.hpp>
#include <wayfire/config.h>
#include <map>
#include <librsvg/rsvg.h>
#include <pango/pango.h>
#include <pango/pangocairo.h>
#include <wayfire/plugins/common/cairo-util.hpp>

#include <sys/stat.h>
#include <unistd.h>
#include <fstream>
#include <bits/stdc++.h>

void drawBase( cairo_t *cr, wf::geometry_t fullRect, int titlePos, int radius ) {
    wf::geometry_t baseRect;

    switch ( titlePos ) {
        /** Left */
        case 1: {
            baseRect.x      = radius;
            baseRect.y      = 0;
            baseRect.width  = fullRect.width - radius;
            baseRect.height = fullRect.height;
            break;
        }

        /** Top */
        case 2: {
            baseRect.x      = 0;
            baseRect.y      = radius;
            baseRect.width  = fullRect.width;
            baseRect.height = fullRect.height - radius;
            break;
        }

        /** Right */
        case 3: {
            baseRect.x      = 0;
            baseRect.y      = 0;
            baseRect.width  = fullRect.width - radius;
            baseRect.height = fullRect.height;
            break;
        }

        /** Bottom */
        case 4: {
            baseRect.x      = 0;
            baseRect.y      = 0;
            baseRect.width  = fullRect.width;
            baseRect.height = fullRect.height - radius;
            break;
        }

        /** No titlebar */
        default: {
            baseRect.x      = 0;
            baseRect.y      = 0;
            baseRect.width  = fullRect.width;
            baseRect.height = fullRect.height;
            break;
        }
    }

    /** Perform the drawing. */
    cairo_rectangle( cr, baseRect.x, baseRect.y, baseRect.width, baseRect.height );
    cairo_fill( cr );
}


void drawRect( cairo_t *cr, wf::geometry_t fullRect, int titlePos, int radius ) {
    /**
     * Declare the rect object.
     * This is the rect that will be drawn at the end
     */
    wf::geometry_t rect;

    /** Get the rectangle co-ordinates */
    switch ( titlePos ) {
        /** Left */
        case 1: {
            rect.x      = 0;
            rect.y      = radius;
            rect.width  = radius;
            rect.height = fullRect.height - 2 * radius;

            break;
        }

        /** Top */
        case 2: {
            rect.x      = radius;
            rect.y      = 0;
            rect.width  = fullRect.width - 2 * radius;
            rect.height = radius;

            break;
        }

        /** Right */
        case 3: {
            rect.x      = fullRect.width - radius;
            rect.y      = radius;
            rect.width  = radius;
            rect.height = fullRect.height - 2 * radius;

            break;
        }

        /** Bottom */
        case 4: {
            rect.x      = radius;
            rect.y      = fullRect.height - radius;
            rect.width  = fullRect.width - 2 * radius;
            rect.height = radius;

            break;
        }

        /** No titlebar */
        default: {
            /** There is nothing to draw. So we get the heck out. */
            return;
        }
    }

    /** Perform the drawing. */
    cairo_rectangle( cr, rect.x, rect.y, rect.width, rect.height );
    cairo_fill( cr );
}


void drawArcs( cairo_t *cr, wf::geometry_t fullRect, int titlePos, int radius ) {
    /**
     * Declare the center, start and end variables.
     * center - center of the circular arc.
     * start  - angle at which the arc begins. (0 = +ve x-axis)
     * end    - angle at which the arc ends.
     */
    wf::point_t center;
    double      start = 0.0;
    double      end   = 0.0;

    /** Top-left corner */
    if ( (titlePos == 1) or (titlePos == 2) ) {
        center = { radius, radius };
        start  = M_PI;
        end    = 3.0 * M_PI / 2.0;

        cairo_move_to( cr, center.x, center.y );
        cairo_line_to( cr, 0, radius );
        cairo_arc( cr, center.x, center.y, radius, start, end );
        cairo_close_path( cr );
        cairo_fill( cr );
    }

    /** Top-right corner */
    if ( (titlePos == 2) or (titlePos == 3) ) {
        center = { fullRect.width - radius, radius };
        start  = 3.0 * M_PI / 2.0;
        end    = 0;

        cairo_move_to( cr, center.x, center.y );
        cairo_line_to( cr, center.x, 0 );
        cairo_arc( cr, center.x, center.y, radius, start, end );
        cairo_close_path( cr );
        cairo_fill( cr );
    }

    /** Bottom-right corner */
    if ( (titlePos == 3) or (titlePos == 4) ) {
        center = { fullRect.width - radius, fullRect.height - radius };
        start  = 0.0;
        end    = M_PI / 2.0;

        cairo_move_to( cr, center.x, center.y );
        cairo_line_to( cr, fullRect.width, center.y );
        cairo_arc( cr, center.x, center.y, radius, start, end );
        cairo_close_path( cr );
        cairo_fill( cr );
    }

    /** Bottom-left corner */
    if ( (titlePos == 4) or (titlePos == 1) ) {
        center = { radius, fullRect.height - radius };
        start  = M_PI / 2.0;
        end    = M_PI;

        cairo_move_to( cr, center.x, center.y );
        cairo_line_to( cr, radius, fullRect.height );
        cairo_arc( cr, center.x, center.y, radius, start, end );
        cairo_close_path( cr );
        cairo_fill( cr );
    }
}


/** Create a z theme with the default parameters */
wf::windecor::decoration_theme_t::decoration_theme_t( std::string app_id, bool isDialog, bool sticky ) {
    mAppId          = app_id;
    this->isDialog  = isDialog;
    this->isSticky = sticky;
    themeMgr        = new IconThemeManager( iconTheme );
}


wf::windecor::decoration_theme_t::~decoration_theme_t() {
    delete themeMgr;
}


/** @return The available height for displaying the title */
int wf::windecor::decoration_theme_t::get_title_height() const {
    return title_height;
}


/** @return The available border for resizing */
int wf::windecor::decoration_theme_t::get_border_size() const {
    return border_size;
}


/*
 * * @return titlebar position
 ** left = 1, top = 2, right = 3, bottom = 4, notitlebar = 0
 */
int wf::windecor::decoration_theme_t::get_titlebar_position() const {
    return title_position;
}


/**
 * Fill the given rectangle with the background color( s ).
 *
 * @param fb The target framebuffer, must have been bound already
 * @param rectangle The rectangle to redraw.
 * @param scissor The GL scissor rectangle to use.
 * @param active Whether to use active or inactive colors
 */
void wf::windecor::decoration_theme_t::render_background( const render_target_t& fb, geometry_t rectangle, const geometry_t& scissor, int state ) const {
    color_t color = (state > 0 ? (state == 1 ? active_color : attn_color) : inactive_color).value();

    int border_radius = (isTiled ? 0 : 5);

    wf::geometry_t tgtRect;

    switch ( title_position ) {
        /** Left */
        case 1: {
            tgtRect = { rectangle.x, rectangle.y, border_radius, rectangle.height };
            break;
        }

        /** Top */
        case 2: {
            tgtRect = { rectangle.x, rectangle.y, rectangle.width, border_radius };
            break;
        }

        /** Right */
        case 3: {
            tgtRect = { rectangle.x + rectangle.width - border_radius, rectangle.y, border_radius, rectangle.height };
            break;
        }

        /** Bottom */
        case 4: {
            tgtRect = { rectangle.x, rectangle.y + rectangle.height - border_radius, rectangle.width, border_radius };
            break;
        }

        default: {
            tgtRect = { 0, 0, 0, 0 };
            break;
        }
    }

    cairo_surface_t *surface = cairo_image_surface_create( CAIRO_FORMAT_ARGB32, rectangle.width, rectangle.height );
    auto            cr       = cairo_create( surface );

    // We want a smooth curve at the corner
    cairo_set_antialias( cr, CAIRO_ANTIALIAS_BEST );

    /* Clear the titlebar background */
    cairo_set_operator( cr, CAIRO_OPERATOR_CLEAR );
    cairo_set_source_rgba( cr, 0, 0, 0, 0 );
    cairo_rectangle( cr, 0, 0, rectangle.width, rectangle.height );
    cairo_fill( cr );

    /* Draw the titlebar */
    cairo_set_operator( cr, CAIRO_OPERATOR_OVER );
    cairo_set_source_rgba( cr, color.r, color.g, color.b, color.a );

    drawBase( cr, rectangle, title_position, border_radius );
    drawRect( cr, rectangle, title_position, border_radius );
    drawArcs( cr, rectangle, title_position, border_radius );

    cairo_destroy( cr );

    wf::simple_texture_t curve;

    cairo_surface_upload_to_texture( surface, curve );
    cairo_surface_destroy( surface );

    OpenGL::render_begin( fb );
    fb.logic_scissor( scissor );
    OpenGL::render_texture( curve.tex, fb, rectangle, glm::vec4( 1.0f ), OpenGL::TEXTURE_TRANSFORM_INVERT_Y );
    OpenGL::render_end();
}


/**
 * Render the given text on a cairo_surface_t with the given size.
 * The caller is responsible for freeing the memory afterwards.
 */
cairo_surface_t * wf::windecor::decoration_theme_t::render_text( std::string text, int width, int height ) const {
    /**
     * If we should not render a titlebar, return a nullptr.
     * This hould not matter, because we will not be rendering
     * the title in such a case.
     */
    if ( not title_position ) {
        return nullptr;
    }

    /**
     * Do not render a title if the available width
     * is less than 20 px, i.e., 10 px padding on either
     * side of the text.
     */
    if ( width <= 20 ) {
        return nullptr;
    }

    const auto      format = CAIRO_FORMAT_ARGB32;
    cairo_surface_t *surface;

    surface = cairo_image_surface_create( format, width, height );
    auto cr = cairo_create( surface );

    std::string fname = font.value();
    double      fsize = ( double )font_size.value();
    wf::color_t frgba = font_color.value();

    // Font Color
    cairo_set_source_rgba( cr, frgba.r, frgba.g, frgba.b, frgba.a );

    PangoFontDescription *font_desc;
    PangoLayout          *layout;

    /** Initiualize font family and size */
    font_desc = pango_font_description_from_string( fname.c_str() );
    pango_font_description_set_absolute_size( font_desc, fsize * PANGO_SCALE );
    pango_font_description_set_gravity( font_desc, PANGO_GRAVITY_AUTO );

    /** Create Pango text layout */
    layout = pango_cairo_create_layout( cr );
    pango_layout_set_font_description( layout, font_desc );
    pango_layout_set_width( layout, (title_position % 2 == 1 ? height - 10 : width - 10) * PANGO_SCALE );

    switch ( title_align ) {
        case 0: {
            pango_layout_set_alignment( layout, PANGO_ALIGN_LEFT );
            break;
        }

        case 1: {
            pango_layout_set_alignment( layout, PANGO_ALIGN_CENTER );
            break;
        }

        case 2: {
            pango_layout_set_alignment( layout, PANGO_ALIGN_RIGHT );
            break;
        }

        default: {
            break;
        }
    }

    pango_layout_set_ellipsize( layout, PANGO_ELLIPSIZE_END );

    PangoContext *pCtxt = pango_layout_get_context( layout );

    pango_context_set_base_gravity( pCtxt, PANGO_GRAVITY_AUTO );

    /** Set the text */
    pango_layout_set_text( layout, text.c_str(), text.size() );

    PangoRectangle logical;

    pango_layout_get_extents( layout, nullptr, &logical );

    /** Handle Left/Right titlebar */
    if ( title_position % 2 == 1 ) {
        /**
         * Reposition before rotation
         */

        int offset = (width - logical.height / PANGO_SCALE) / 2;
        cairo_move_to( cr, offset, height - 5 );

        /** Rotate the surface */
        cairo_rotate( cr, -M_PI / 2.0 );
    }

    else {
        /** 10 px padding to the icon */
        int offset = (height - logical.height / PANGO_SCALE) / 2;
        cairo_move_to( cr, 5, offset );
    }

    /** Render the text */
    pango_cairo_show_layout( cr, layout );

    /** Release the font_description object */
    pango_font_description_free( font_desc );

    /** Release the layout */
    g_object_unref( layout );

    /** Release the cairo_context object */
    cairo_destroy( cr );

    /** Return the object */
    return surface;
}


cairo_surface_t * wf::windecor::decoration_theme_t::get_button_surface( button_type_t button, const button_state_t& state ) const {
    /** Let's create a button surface first */
    cairo_surface_t *button_surface = cairo_image_surface_create( CAIRO_FORMAT_ARGB32, state.width, state.height );

    /** Then cairo context */
    auto cr = cairo_create( button_surface );

    /** Now, we enable antialiasing */
    cairo_set_antialias( cr, CAIRO_ANTIALIAS_BEST );

    /* Clear the button background */
    cairo_set_operator( cr, CAIRO_OPERATOR_CLEAR );
    cairo_set_source_rgba( cr, 0, 0, 0, 0 );
    cairo_rectangle( cr, 0, 0, state.width, state.height );
    cairo_fill( cr );

    /** Set the render mode to CAIRO_OPERATOR_OVER */
    cairo_set_operator( cr, CAIRO_OPERATOR_OVER );

    /** We should not render a BG for an icon */
    if ( button == BUTTON_ICON ) {
        cairo_set_source_rgba( cr, 0.0, 0.0, 0.0, 0.0 );
        cairo_rectangle( cr, 0.0, 0.0, state.width, state.height );
        cairo_fill( cr );

        /** Get the icon path */
        std::string iconPath = themeMgr->iconPathForAppId( mAppId );

        /** Icon surface */
        cairo_surface_t *button_icon;

        /* Draw svg using rsvg on the icon surface */
        if ( iconPath.find( ".svg" ) != std::string::npos ) {
            GFile      *svgFile = g_file_new_for_path( iconPath.c_str() );
            RsvgHandle *svg     = rsvg_handle_new_from_gfile_sync( svgFile, RSVG_HANDLE_FLAGS_NONE, NULL, NULL );

            button_icon = cairo_image_surface_create( CAIRO_FORMAT_ARGB32, state.width, state.height );
            auto          crsvg = cairo_create( button_icon );
            RsvgRectangle rect{ 0, 0, state.width, state.height };
            rsvg_handle_render_document( svg, crsvg, &rect, nullptr );
            cairo_destroy( crsvg );

            g_object_unref( svg );
            g_object_unref( svgFile );
        }

        /* Draw png on the icon surface */
        else {
            button_icon = cairo_image_surface_create_from_png( iconPath.c_str() );
        }

        cairo_scale( cr,
                     1.0 * state.width / cairo_image_surface_get_width( button_icon ),
                     1.0 * state.height / cairo_image_surface_get_height( button_icon )
        );

        /** Render the icon on the button surface */
        cairo_set_source_surface( cr, button_icon, 0, 0 );
        cairo_paint( cr );

        cairo_destroy( cr );

        return button_surface;
    }

    /** A gray that looks good on light and dark themes */
    color_t base = { 0.60, 0.60, 0.63, 0.36 };

    /**
     * We just need the alpha component.
     * r == g == b == 0.0 will be directly set
     */
    double hover = 0.27;

    /** Coloured base on hover/press. Don't compare float to 0 */
    if ( fabs( state.hover_progress ) > 1e-3 ) {
        /** Choose the corect color for the correct button */
        switch ( button ) {
            case BUTTON_CLOSE: {
                base = close_color.value();
                break;
            }

            case BUTTON_TOGGLE_MAXIMIZE: {
                base = maximize_color.value();
                break;
            }

            case BUTTON_MINIMIZE: {
                base = minimize_color.value();
                break;
            }

            case BUTTON_STICKY: {
                base = sticky_color.value();
                break;
            }

            default: {
                assert( false );
            }
        }

        /** Draw the base */
        cairo_set_source_rgba(
            cr,
            base.r,
            base.g,
            base.b,
            base.a + hover * state.hover_progress
        );
        cairo_arc( cr, state.width / 2, state.height / 2, state.width / 2, 0, 2 * M_PI );
        cairo_fill( cr );

        /** Draw the border only when we hover over the button or press it */
        cairo_set_line_width( cr, state.border );
        cairo_set_source_rgba( cr, 0.00, 0.00, 0.00, 1.5 * hover * state.hover_progress );

        // This renders great on my screen( 110 dpi 1376x768 lcd screen )
        // How this would appear on a Hi-DPI screen is questionable
        double r = state.width / 2 - 0.5 * state.border;
        cairo_arc( cr, state.width / 2, state.height / 2, r, 0, 2 * M_PI );
        cairo_stroke( cr );
    }

    else {
        if ( (button == BUTTON_STICKY) && isSticky ) {
            color_t base = sticky_color.value();

            /** Draw the base */
            cairo_set_source_rgba(
                cr,
                base.r,
                base.g,
                base.b,
                base.a + hover * state.hover_progress
            );
            cairo_arc( cr, state.width / 2, state.height / 2, state.width / 2, 0, 2 * M_PI );
            cairo_fill( cr );

            /** Draw a point at the center to indicate that the view is sticky */
            cairo_set_line_width( cr, state.border );
            cairo_arc( cr, state.width / 2, state.height / 2, 2, 0, 2 * M_PI );
            cairo_fill( cr );

            // This renders great on my screen( 110 dpi 1376x768 lcd screen )
            // How this would appear on a Hi-DPI screen is questionable
            double r = state.width / 2 - 0.5 * state.border;
            cairo_arc( cr, state.width / 2, state.height / 2, r, 0, 2 * M_PI );
            cairo_stroke( cr );
        }

        else {
            /** Draw a gray base */
            cairo_set_source_rgba( cr, base.r, base.g, base.b, base.a );
            cairo_arc( cr, state.width / 2, state.height / 2, state.width / 2, 0, 2 * M_PI );
            cairo_fill( cr );

            /** Draw a border */
            cairo_set_line_width( cr, state.border );
            cairo_set_source_rgba( cr, 0.00, 0.00, 0.00, 1.5 * hover );

            // This renders great on my screen( 110 dpi 1376x768 lcd screen )
            // How this would appear on a Hi-DPI screen is questionable
            double r = state.width / 2 - 0.5 * state.border;
            cairo_arc( cr, state.width / 2, state.height / 2, r, 0, 2 * M_PI );
            cairo_stroke( cr );
        }
    }

    cairo_destroy( cr );

    return button_surface;
}
